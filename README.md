Extends the settings menu, adding additional types for configuration values.

New types (registered under `window.Azzu.SettingsTypes`):

|Type|Explanation|
|---|---|
|`KeyBinding`|Allows the user to enter a key into the input field, while holding down any modifiers like Ctrl or Alt. His input will be saved in the setting.|
|`MouseButtonBinding`|Allows the user to click in the input field with his mouse, while holding down any modifiers like Ctrl or Alt. His input will be saved in the setting.|
|`FilePicker[FileType]`|`[FileType]` has to be one of `Image`, `Video`, `ImageVideo` or `Audio`, e.g. `FilePickerImage`. The setting will create an input that allows the user to select a file of the specified type.|
|`DirectoryPicker`|Same as a FilePicker but instead picks directories.|

![Demonstration](doc/settings-extender.mp4)

## Installation

Download the [`src/settings-extender.js`](https://gitlab.com/foundry-azzurite/settings-extender/raw/master/src/settings-extender.js) and put it in your module directory. Add this js file to the `scripts` property in your module's `module.json`.

Load order is irrelevant because the types are initialized before the `game.settings` object. So if you use them to register game settings, you can never run into any issues, and it doesn't make sense to use them before.

It's safe to be repeatedly included, so no conflicts will arise from multiple modules using it. The API will not be changed (except if the FVTT settings window changes drastically, at which point you would have to do changes anyway if you did it yourself) so you don't need to worry about anything breaking. 

## Usage

Register a setting with one of the above types, one of `KeyBinding`, `MouseButtonBinding`, `FilePickerImage
`, `FilePickerVideo`, `FilePickerImageVideo`, `FilePickerAudio` or `DirectoryPicker`:

```javascript
game.settings.register('moduleName', 'settingKey', {
	name: 'Bind a Key',
	hint: 'Enter a key here that should do something.',
	type: window.Azzu.SettingsTypes.KeyBinding,
	default: 'x',
	scope: 'client',
	config: true
});
```

The types will save their settings as a `String` value. You can either use this value directly or use the `Type.parse()` method to get the setting into a more programmer-friendly format.

Example:

```javascript
const stringValue = game.settings.get('moduleName', 'settingKey');
const parsedValue = KeyBinding.parse(stringValue);
window.addEventListener('keydown', (event) => {
	if (KeyBinding.eventIsForBinding(event, parsedValue)) {
		// do something
	}
});
```

### Type API

#### All types

* **`type.parse(val)`:** parses a value saved in `game.settings` into a more programming-friendly value
* **`type.format(val)`:** formats the previous programming-friendly value into a more human-readable one. The return value of this is saved into `game.settings`.

#### FilePicker[FileType]

* **`FilePicker.format(val)`:** Returns the same string, does no formatting
  * Example: "icons/d20.png"
* **`FilePicker.parse(val)`:** Returns the same string, does no parsing

#### DirectoryPicker

* **`DirectoryPicker.format(val)`:** Returns the same string, does no formatting
  * Example: "modules/pings"
* **`FilePicker.parse(val)`:** Returns the same string, does no parsing

#### KeyBinding

* **`KeyBinding.parse(val)`:**
  * `val` is a string returned by `KeyBinding.format(val)`
  * Returns an object with the following structure:
    ```
    {
    	key: String,       // the value of the KeyboardEvent.key property
    	ctrlKey: Boolean,  // the value of the KeyboardEvent.ctrlKey property
    	shiftKey: Boolean, // the value of the KeyboardEvent.shiftKey property
    	altKey: Boolean,   // the value of the KeyboardEvent.altKey property
    	metaKey: Boolean   // the value of the KeyboardEvent.metaKey property
    }
    ```
* **`KeyBinding.format(val)`:** 
  * `val` can either be a string returned by `KeyBinding.parse(val)` or a [KeyboardEvent](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent)
  * Returns a string with one or multiple of the following prefixes "Ctrl + ", "Shift + ", "Alt + " or "Meta + ", followed by the value of the `val.key` property. 
  * Example: `Ctrl + c`
* **`Keybinding.eventIsForBinding(event, parsedValue)`:** 
  * `event` can be a [jQuery Event](https://api.jquery.com/category/events/event-object/) or a [KeyboardEvent](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent)
  * `parsedValue` is a value returned by `KeyBinding.parse(val)`
  * Returns true if the event was triggered by the given key binding
  
#### MouseButtonBinding

* **`MouseButtonBinding.parse(val)`:** 
  * Returns an object with the following structure (see [MouseEvent.button](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button)):
    ```
    {
    	button: Number,    // the value of the MouseEvent.button property
    	ctrlKey: Boolean,  // the value of the MouseEvent.ctrlKey property
    	shiftKey: Boolean, // the value of the MouseEvent.shiftKey property
    	altKey: Boolean,   // the value of the MouseEvent.altKey property
    	metaKey: Boolean   // the value of the MouseEvent.metaKey property
    }
    ```
* **`MouseButtonBinding.format(val)`:**
  * `val` can either be a string returned by `MouseButtonBinding.parse(val)` or a [MouseEvent](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent)
  * Returns a string with one or multiple of the following prefixes "Ctrl + ", "Shift + ", "Alt + " or "Meta + ", followed by the mouse key pressed: either "LeftClick", "MiddleClick", "RightClick" or "Mouse[X]", where "[X]" is a number greater than 3 (for mouses with more than 3 buttons). 
  * Example: "Shift + Mouse4" or "Alt + LeftClick"
* **`Keybinding.eventIsForBinding(event, parsedValue)`:** 
  * `event` can be a [jQuery Event](https://api.jquery.com/category/events/event-object/) or a [MouseEvent](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent)
  * `parsedValue` is a value returned by `KeyBinding.parse(val)`
  * Returns `true` if the event was triggered by the given key binding
