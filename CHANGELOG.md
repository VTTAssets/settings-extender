# 1.1.2

* 0.5.4 compatibility

# 1.1.1

* Extend `SettingsConfig` properly by adding a `baseApplication` = `SettingsConfig` option
* Make sure to always load the latest version of the library

# 1.1.0

* Add `DirectoryPicker` type
